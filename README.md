## xylophone music app

This project demonstrates the use of Flutter packages, integrating the same in the app and using all the extensive package features in the app

Xylophone app plays music using the AudioCache() feature.

This project also demonstrates the used of functions with parameters and arguments.

![xylophone](images/img.png)

