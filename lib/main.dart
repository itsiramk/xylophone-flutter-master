import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(XylophoneApp());
void playsound(int noteNumber) {
  final player = AudioCache();
  player.play('note$noteNumber.wav');
}

Expanded buildNoteKey({int soundNumber, Color colors}) {
  //braces help to u annotate the variable when being called ..see below
  return Expanded(
    child: TextButton(
      onPressed: () {
        playsound(soundNumber);
      },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(colors),
      ),
    ),
  );
}

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildNoteKey(soundNumber: 1, colors: Colors.red),
                buildNoteKey(soundNumber: 2, colors: Colors.yellow),
                buildNoteKey(soundNumber: 3, colors: Colors.blue),
                buildNoteKey(soundNumber: 4, colors: Colors.deepOrange),
                buildNoteKey(colors: Colors.indigo, soundNumber: 5),
                buildNoteKey(colors: Colors.green, soundNumber: 6),
                buildNoteKey(colors: Colors.purple, soundNumber: 7),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
